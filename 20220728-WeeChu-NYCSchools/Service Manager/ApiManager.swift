//
//  ApiManager.swift
//  20220728-WeeChu-NYCSchools
//
//  Created by Coding on 7/29/22.
//

import Foundation

class ApiManager {
    //set url address
    static let schoolListURL = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    
    static let schoolDetailsURL = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    
    //calling api
    func getData<T:Decodable>(urlString: String, completion: @escaping (_ data:[T], _ error: String?) -> Void) {
        guard let url = URL(string: urlString) else {
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data,
                  let response = response as? HTTPURLResponse, response.statusCode == 200,
                  error == nil else {
                      return
                  }
            
            // Decode
            do {
                let data = try JSONDecoder().decode([T].self, from: data)
                completion(data, nil)
            }
            catch {
                print(error)
            }
            
        }
        
        task.resume()
    }
    
    
}
