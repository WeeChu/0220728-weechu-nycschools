//
//  SchoolDetailsViewController.swift
//  20220728-WeeChu-NYCSchools
//
//  Created by Coding on 7/29/22.
//

import UIKit

class SchoolDetailsViewController: UIViewController {
    
    var schoolName = ""
    let schoolNameLabel = UILabel()
    let satMathLabel = UILabel()
    let satReadingLabel = UILabel()
    let satWritingLabel = UILabel()
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .yellow
        view.addSubview(schoolNameLabel)
        view.addSubview(satMathLabel)
        view.addSubview(satReadingLabel)
        view.addSubview(satWritingLabel)
        let viewModel = SchoolDetailsViewModel(schoolName: schoolName)
        viewModel.fetchSchool()
        
        // reload UI
        viewModel.reloadUIClosure = {[weak self] in
            DispatchQueue.main.async {
                guard let selectedSchool = viewModel.getDisplaySchool() else {
                    return
                }
                self?.satMathLabel.text = "SAT Math Score : \(selectedSchool.sat_math_avg_score)"
                self?.satReadingLabel.text = "SAT Reading Score : \(selectedSchool.sat_critical_reading_avg_score)"
                self?.satWritingLabel.text = "SAT Writing Score : \(selectedSchool.sat_writing_avg_score)"
            }
        }
        schoolNameLabel.text = schoolName
        schoolNameLabel.numberOfLines = 0

    }
    
    //will use autolayout if I have more time
    override func viewDidLayoutSubviews() {
        schoolNameLabel.frame = CGRect(x: 10, y: 10, width: view.frame.size.width, height: 80)
        satMathLabel.frame = CGRect(x: 10, y: 130, width: view.frame.size.width, height: 20)
        satReadingLabel.frame = CGRect(x: 10, y: 160, width: view.frame.size.width, height: 20)
        satWritingLabel.frame = CGRect(x: 10, y: 190, width: view.frame.size.width, height: 20)
        
    }
}
