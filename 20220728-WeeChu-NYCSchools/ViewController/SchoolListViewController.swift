//
//  ViewController.swift
//  20220728-WeeChu-NYCSchools
//
//  Created by Coding on 7/29/22.
//

import UIKit

class SchoolListViewController: UIViewController {
    
    let viewModel = SchoolListViewModel()
    let table = UITableView()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .yellow
        viewModel.fetchSchool()
        view.addSubview(table)
        table.register(SchoolListTableViewCell.self, forCellReuseIdentifier: SchoolListTableViewCell.indentifier)
        table.delegate = self
        table.dataSource = self
        
        // reload UI
        viewModel.tableReloadClosure = {[weak self] in
            DispatchQueue.main.async {
                self?.table.reloadData()
            }
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        table.frame = view.bounds
    }


}

extension SchoolListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRow()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = table.dequeueReusableCell(withIdentifier: SchoolListTableViewCell.indentifier, for: indexPath) as! SchoolListTableViewCell
        let displayCell = viewModel.getDisplayCells()
        cell.schoolNameLabel.text = displayCell[indexPath.row].school_name
        cell.schoolEmailLabel.text = "Email : \(displayCell[indexPath.row].school_email ?? "Not Available")"
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
}

extension SchoolListViewController: SchoolListTableViewCellDelegate {
    func buttonDidTapped(schoolName: String) {
        let vc = SchoolDetailsViewController()
        vc.schoolName = schoolName
        self.present(vc, animated: true, completion: nil)
    }
    
    
}

