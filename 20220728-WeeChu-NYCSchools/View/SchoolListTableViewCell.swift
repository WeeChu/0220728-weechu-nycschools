//
//  SchoolListTableViewCell.swift
//  20220728-WeeChu-NYCSchools
//
//  Created by Coding on 7/29/22.
//

import UIKit


// use delegate pattern to pass data if button tapped
protocol SchoolListTableViewCellDelegate: AnyObject {
    func buttonDidTapped(schoolName: String)
}

class SchoolListTableViewCell: UITableViewCell {
    
    static let indentifier = "customCell"
    weak var delegate: SchoolListTableViewCellDelegate?
    
    let schoolNameLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    let schoolEmailLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    let button: UIButton = {
        let button = UIButton()
        button.setTitle("Show Details", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .link
        button.layer.cornerRadius = 10
        return button
    }()
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(schoolNameLabel)
        contentView.addSubview(schoolEmailLabel)
        contentView.addSubview(button)
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
    }
    
    @objc func buttonTapped() {
        delegate?.buttonDidTapped(schoolName: schoolNameLabel.text ?? "Not Available")
    }
    
    //will use autolayout if I have more time
    override func layoutSubviews() {
        schoolNameLabel.frame = CGRect(x: 5, y: 10, width: contentView.frame.size.width, height: 50)
        schoolEmailLabel.frame = CGRect(x: 5, y: 80, width: contentView.frame.size.width, height: 20)
        button.frame = CGRect(x: 5, y: 120, width: 180, height: 30)

    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
