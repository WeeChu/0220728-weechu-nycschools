//
//  Model.swift
//  20220728-WeeChu-NYCSchools
//
//  Created by Coding on 7/29/22.
//

import Foundation

struct SchoolList: Decodable {
    let school_name: String?
    let school_email: String?
}

struct SchoolDetails: Decodable {
    let school_name: String
    let sat_critical_reading_avg_score: String
    let sat_math_avg_score: String
    let sat_writing_avg_score: String
}
