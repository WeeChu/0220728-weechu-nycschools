//
//  SchoolDetailsViewModel.swift
//  20220728-WeeChu-NYCSchools
//
//  Created by Coding on 7/29/22.
//

import Foundation

class SchoolDetailsViewModel {
    let apiManager: ApiManager!
    var schoolName: String
    
    private var displaySchool: SchoolDetails? {
        didSet {
            self.reloadUIClosure?()
            
        }
    }
    
    var reloadUIClosure: (() -> ())?
    
    init(apiManager: ApiManager = ApiManager(), schoolName: String) {
        self.apiManager = apiManager
        self.schoolName = schoolName
    }
    
    func fetchSchool() {
        self.apiManager.getData(urlString: ApiManager.schoolDetailsURL) { [weak self](data:[SchoolDetails]?, error) in
            guard let data = data, error == nil else {
                return
            }
            
            self?.parseData(list: data)
        }
    }
    
    func parseData(list: [SchoolDetails]) {
        
        let uppercasedSchool = schoolName.uppercased()
        
        for school in list {
            if school.school_name == uppercasedSchool {
                displaySchool = school
                return
            }
        }
        displaySchool = SchoolDetails(school_name: "School Not Available", sat_critical_reading_avg_score: "Score Not Available", sat_math_avg_score: "Score Not Available", sat_writing_avg_score: "Score Not Available")
    }
    
    func getDisplaySchool() -> SchoolDetails? {
        return displaySchool
    }
}
