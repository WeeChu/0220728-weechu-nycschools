//
//  SchoolListViewModel.swift
//  20220728-WeeChu-NYCSchools
//
//  Created by Coding on 7/29/22.
//

import Foundation

class SchoolListViewModel {
    let apiManager: ApiManager!
    
    
    //binding
    private var displayCells = [SchoolList]() {
        didSet {
            self.tableReloadClosure?()
        }
    }
    
    //use this to pass data
    var tableReloadClosure: (() -> ())?
    
    init(apiManager: ApiManager = ApiManager()) {
        self.apiManager = apiManager
    }
    
    // fetch data
    func fetchSchool() {
        self.apiManager.getData(urlString: ApiManager.schoolListURL) { [weak self](data:[SchoolList]?, error) in
            guard let data = data, error == nil else {
                return
            }
            
            self?.parseData(list: data)
        }
    }
    
    //parse and process data
    func parseData(list: [SchoolList]) {
        var cells = [SchoolList]()
        
        for school in list {
            cells.append(school)
        }
        
        displayCells = cells
        
    }
    
    // return the number needed for row
    func numberOfRow() -> Int {
        return displayCells.count
    }
    
    //return the display cell which set to private
    func getDisplayCells() -> [SchoolList] {
        return displayCells
    }
}
